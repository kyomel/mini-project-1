require('dotenv').config();
const express = require('express');
const cors = require('cors');
const morgan = require('morgan');
const app = express();
const port = process.env.PORT || 3000;

const router = require('./router');
const exceptionHandler = require('./Middleware/exceptionHandler');

app.use(morgan('tiny'));
app.use(cors());
app.use(express.json());

// Router
app.use('/api/v1', router);

exceptionHandler.forEach(err => app.use(err))


app.listen(port, ()=>console.log(`Listening on PORT: ${port}`));