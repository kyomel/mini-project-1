const {users} = require('../models');
const jwt = require('jsonwebtoken');

module.exports = async function(req, res, next) {
    try {
        let token = req.headers.authorization;
        payload = await jwt.verify(token, "secretkey");
        let user = await users.findByPk(payload.id);
        req.user = user;
        next()
    } catch (err) {
        console.log(err)
        res.status(401).json({
            status: "Fail",
            Message: "Unauthorized Login"
        })
    }
}