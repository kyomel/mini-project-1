const models = require('../models');

const errorHandler = require('../Helper/errorHandler')
module.exports = modelsName => {
    const model = models[modelsName];
    return async function(req, res, next) {
        try {
            let instance = await model.findByPk(req.params.id);
            console.log(instance.id)
            if(instance.user_id !== req.user.id) {
                errorHandler(res, 403, `this ${modelsName} is not yours`)
            } else {
                req.instance = instance;
                next();
            }
        } catch (err) {
            res.status(400);
            next(err)
        }
    }
}