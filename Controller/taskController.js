const {task} = require('../models');

const successHandler = require('../Helper/successHandler');
const paginationHandler = require('../Helper/paginationhandler');
const errorHandler = require('../Helper/errorHandler');
const sorted = require('../Helper/sortedByDate');

class Tasks {
    static async create(req, res){
        try {
            const tasks = await task.create({
                name: req.body.name,
                description: req.body.description,
                dueDate: req.body.dueDate,
                user_id: req.user.id
            }) 
            successHandler(res, 200, tasks)
        } catch (err) {
            errorHandler(res, 400, 'Please complete all the requirements')            
        }
    }

    static async show(req, res) {
        try {
           const tasks = await task.findAll({
               include: 'author',
           });
            const result = paginationHandler(req, sorted(tasks))
            successHandler(res, 200, result)
        } catch (err) {
            throw new Error('Internal Error')
        }
    }

    static async filteredCompletion(req, res) {
        try {
            const tasks = await task.findAll({
                where: {completion: true},
                include: 'author'
            })
            let result = paginationHandler(req, sorted(tasks))
            successHandler(res, 200, result) 
        } catch (err) {
            throw new Error('Internal Error')
        }
    }
    static async filteredImportance(req, res) {
        try {
            const tasks = await task.findAll({
                where: {importance: true},
            })
            include: 'author'
            let result = paginationHandler(req, sorted(tasks));
            successHandler(res, 200, result)
        } catch (err) {
            throw new Error('Internal Error')
        }
    }
    static async toogleImportance(req, res) {
        try {
            if(req.instance.importance == false) {
                await task.update({importance: true}, {where: {id: req.params.id}})
                successHandler(res, 201, `id task: ${req.params.id} has been updated`) 
            } else if(req.instance.importance == true) {
                await task.update({importance: false}, {where: {id: req.params.id}})
                successHandler(res, 201, `id task: ${req.params.id} has been updated`) 
            }
        } catch(err) {
            errorHandler(res, 400, err.message)
        }
    }
    static async toogleCompletion(req, res) {
        try {
            if(req.instance.completion == false) {
                await task.update({completion: true}, {where: {id: req.params.id}, include: 'author'})
                successHandler(res, 201, `id task: ${req.params.id} has been updated`) 
            } else if(req.instance.completion == true) {
                await task.update({completion: false}, {where: {id: req.params.id}, include: 'author'})
                successHandler(res, 201, `id task: ${req.params.id} has been updated`)   
                }   
        } catch (err) {
            errorHandler(res, 400, err.message)
        }
        }
    static async update(req, res) {
        try {
            await task.update(req.body, {
                where: {id: req.params.id},
                include: 'author'
            })
            successHandler(res, 201, `ID: ${req.params.id} has been updated`)
        } catch (err) {
            errorHandler(res, 402, err.message)
        }
    }
    
    static async delete(req, res){
        try {
            await task.destroy({
                where: {id: req.params.id},
                include: 'author'
            });
            successHandler(res, 201, `ID: ${req.params.id} has been deleted`)
        } catch (err) {
            errorHandler(res, 402, err.message)
        }
    }
}

module.exports = Tasks;