'use strict';
module.exports = (sequelize, DataTypes) => {
  const task = sequelize.define('task', {
    name: DataTypes.STRING,
    description: DataTypes.TEXT,
    completion:{
      type: DataTypes.BOOLEAN,
      defaultValue: false
    },
    importance: {
      type: DataTypes.BOOLEAN,
      defaultValue: false
    },
    dueDate: {
     type: DataTypes.DATE,
     defaultValue: Date.now()
    },
    user_id: DataTypes.INTEGER
  }, {});
  task.associate = function(models) {
    task.belongsTo(models.users, {
      foreignKey: "user_id",
      as: "author"
    })
  };
  return task;
};