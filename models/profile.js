'use strict';
module.exports = (sequelize, DataTypes) => {
  const profile = sequelize.define('profile', {
    name: DataTypes.STRING,
    image: {
      type: DataTypes.TEXT,
      defaultValue: 'https://ik.imagekit.io/xn7pla5njr/gerandong_oPwq2Fei_.jpeg'
    },
    user_id: DataTypes.INTEGER
  }, {});
  profile.associate = function(models) {
    profile.belongsTo(models.users, {
      foreignKey: 'user_id',
      as: 'owner'
    })
  };
  return profile;
};