module.exports = function(res, code, data) {
    let result = res.status(code).json({
                 status: "Success",
                 data
                })
    return result
};