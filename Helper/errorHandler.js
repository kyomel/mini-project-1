module.exports = (res, code, message) => {
    res.status(code).json({
        status: "Fail",
        Message: message
    })
}